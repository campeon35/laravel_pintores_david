<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pintor;

class PintoresController extends Controller
{
    public function getTodos() {
		$pintores = Pintor::all();
		return view('pintores.index', array('pintores' => $pintores));
	}

	public function getVer($id){
		$pintor = Pintor::findOrFail($id);
		return view('pintores.mostrar', array('pintor' => $pintor));
	}
}
