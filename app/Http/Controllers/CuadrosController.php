<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pintor;
use App\Cuadro;

class CuadrosController extends Controller
{
    public function getCrear() {
    	$pintores = Pintor::all();
		return view('cuadros.crear', array('pintores' => $pintores),);
	}
	
	public function postCrear(Request $request) {
		$c = new Cuadro();

		$c->nombre = $request->nombre;
		$c->pintor_id = Pintor::findOrFail($request->pintor)->id;
		
		if (!empty($request->imagen) && $request->imagen->isValid()) {
			$c->imagen = $request->imagen->store('', 'cuadros');
		}
		
		try{
			$c->save();
			return redirect('pintores')->with('mensaje', "Cuadro: $c->cuadro guardado");

		} catch(\Illuminate\Database\QueryException $ex){
			return redirect('pintores')->with('mensaje', "Error añadiendo cuadro");
		}
	}
}
