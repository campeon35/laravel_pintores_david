@extends('layouts.master')

@section('titulo')
	Nuevo cuadro
@endsection

@section('contenido')
	<div class="row">
		<div class="offset-md-3 col-md-6">
			<div class="card">
				<div class="card-header text-center">
					Añadir cuadro
				</div>
				<div class="card-body" style="padding:30px">
					<form action="{{ action('CuadrosController@postCrear') }}" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="nombre">Nombre del cuadro</label>
							<input type="text" name="nombre" id="nombre" class="form-control">
						</div>
						<div class="form-group">
							<label for="pintor">Pintor</label>
							<select name="pintor" class="form-control">
								@foreach($pintores as $pintor)
									<option value="{{ $pintor->id }}">{{ $pintor->nombre }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="imagen">Imagen</label>
							<br>
							<input type="file" name="imagen" id="imagen">
						</div>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success" style="padding:8px 100px; margin-top:25px;">
								Añadir cuadro
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection