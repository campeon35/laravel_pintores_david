@extends('layouts.master')

@section('titulo')
	Pintores
@endsection

@section('contenido')
	<h2>Todos los pintores:</h2>
	
	<div class="row">
		<table>
			<tr>
				<th>Nombre</th>
				<th>País</th>
				<th>Cuadros</th>
			</tr>
			@foreach($pintores as $clave => $pintor)
				<tr>
					<td>
						<a href="{{ url('/pintores/mostrar/' . $pintor->id ) }}">
							{{ $pintor->nombre }}
						</a>
					</td>
					<td>
						{{ $pintor->pais }}
					</td>
					<td>
						{{ count($pintor->cuadros) }}
					</td>
				</tr>
			@endforeach
		</table>
	</div>
@endsection