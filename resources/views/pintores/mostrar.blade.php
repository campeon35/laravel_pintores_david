@extends('layouts.master')

@section('titulo')
	Descripcion de pintor 
@endsection

@section('contenido')
	<h1>{{ $pintor->nombre }}</h1>
	<h3>País: {{ $pintor->pais }}</h3>
	<h3>Cuadros:</h3>
	<div class="row">
		<div class="col-sm-3">
			@foreach($pintor->cuadros as $indice => $valor)
				<div>
					<p>{{ $valor->nombre }}</p>
					<img src="../../assets/cuadros/{{ $valor->imagen }}">
				</div>
			@endforeach
		</div>
	</div>
@endsection